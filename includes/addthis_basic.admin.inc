<?php

/**
 * @file
 * Admin settings for AddThis Basic.
 */

function addthis_basic_admin_settings_form($form_state) {

  $form['addthis_basic_script_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Script URL'),
    '#default_value' => variable_get('addthis_basic_script_url', '//s7.addthis.com/js/300/addthis_widget.js'),
  );

  $form['addthis_basic_publisher_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Publisher ID'),
    '#default_value' => variable_get('addthis_basic_publisher_id', ''),
  );

  $form['addthis_basic_admin_pages'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include AddThis script on admin pages'),
    '#default_value' => variable_get('addthis_basic_admin_pages', FALSE),
  );

  $form['addthis_basic_twitter_via'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter via handle'),
    '#default_value' => variable_get('addthis_basic_twitter_via', FALSE),
  );

  $form['visibility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pages'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['visibility']['addthis_basic_visibility'] = array(
    '#type' => 'radios',
    '#options' => array(
      'exclude' => t('All pages except those listed'),
      'include' => t('Only the listed pages'),
    ),
    '#default_value' => variable_get('addthis_basic_visibility', 'exclude'),
  );

  $form['visibility']['addthis_basic_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Include script on specific pages'),
    '#default_value' => variable_get('addthis_basic_pages', ''),
    '#description' => t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array(
      '%blog' => 'blog',
      '%blog-wildcard' => 'blog/*',
      '%front' => '<front>'
    )),
  );

  return system_settings_form($form);
}
