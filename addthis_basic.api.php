<?php

/**
 * @file
 * API documentation for AddThis Basic.
 */

/**
 * Allows modules to add items to the addthis_share variable.
 *
 * @see http://www.addthis.com/academy/setting-the-url-title-to-share/
 *
 * @param array $share_custom
 *  Associat
 *
 */
function hook_addthis_basic_share_alter(&$share_custom) {
  // See http://www.addthis.com/academy/changes-to-how-twitter-works-with-addthis/
  $share_custom['passthrough']['twitter']['via'] = 'drupal';
}
